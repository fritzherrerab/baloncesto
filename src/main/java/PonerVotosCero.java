import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class PonerVotosCero  extends HttpServlet {
    
    private ModeloDatos bd;

    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
        HttpSession s = req.getSession(true);

        bd.ponerVotosCero();

        s.setAttribute("respuesta", "Is DONE..");

        res.sendRedirect(res.encodeRedirectURL("TablaVotoCero.jsp"));

    }


    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
